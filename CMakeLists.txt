cmake_minimum_required(VERSION 3.20)
project(use-cmake-lib)
add_executable(use-cmake-lib use-cmake-lib.c)
# we want to link against libhwcmake
target_link_libraries(use-cmake-lib hw-cmake-so-a internal-cmake-so-a)
install(TARGETS use-cmake-lib RUNTIME DESTINATION bin)
